import re
from setuptools import setup
 
 
with open("README.txt", "rb") as f:
    long_descr = f.read().decode("utf-8")
 
 
setup(
    name = "slack_emoji_watcher",
    packages = ["slack_emoji_watcher", "slack_emoji_watcher.util.rest_client", "slack_emoji_watcher.util"],
    entry_points = {
        "console_scripts": ['slack_emoji_watcher = slack_emoji_watcher.main:main']
        },
    version = '0.0.1',
    description = "Watch slack for emojis",
    long_description = long_descr,
    author = "Charlie Wolf",
    author_email = "charlie@wolf.is",
    install_requires = ('pyyaml', 'websocket-client', 'telethon', 'requests')
)
