try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin
from requests.exceptions import HTTPError
from requests import Session


class PrefixedSession(Session):

    def __init__(self, **kwargs):
        self.prefix_url = kwargs.pop('base_url')
        super(PrefixedSession, self).__init__(**kwargs)

    def request(self, method, url, *args, **kwargs):
        url = urljoin(self.prefix_url, url)
        return super(PrefixedSession, self).request(method, url, *args, **kwargs)


class FlaskConfigurablePrefixedSession(Session):

    def __init__(self, **kwargs):
        self.config_key = kwargs.pop('prefix_config_key')
        self.default_prefix_url = kwargs.pop('base_url')
        super(FlaskConfigurablePrefixedSession, self).__init__(**kwargs)

    def request(self, method, url, *args, **kwargs):
        from flask import current_app
        url = urljoin(current_app.config.get(self.config_key, self.default_prefix_url), url)
        return super(FlaskConfigurablePrefixedSession, self).request(method, url, *args, **kwargs)


class GenericRestAPI(object):
    BASE_URL = None
    SESSION_CLASS = PrefixedSession

    def __init__(self):
        self._session_args = getattr(self, '_session_args', dict())
        if self.SESSION_CLASS == PrefixedSession:
            self._session_args['base_url'] = self.BASE_URL
        elif self.SESSION_CLASS == FlaskConfigurablePrefixedSession:
            self._session_args['prefix_config_key'] = self.CONFIG_KEY
            self._session_args['base_url'] = self.BASE_URL
        self._setup_session()

    def _setup_session(self):
        self._session = self.SESSION_CLASS(**self._session_args)

    def get(self, *args, **kwargs):
        return self._request('GET', *args, **kwargs)

    def post(self, *args, **kwargs):
        return self._request('POST', *args, **kwargs)

    def put(self, *args, **kwargs):
        return self._request('PUT', *args, **kwargs)

    def delete(self, *args, **kwargs):
        return self._request('DELETE', *args, **kwargs)

    def patch(self, *args, **kwargs):
        return self._request('PATCH', *args, **kwargs)

    def _request(self, method, url, *args, **kwargs):
        return self._session.request(method, url, *args, **kwargs)


class BasicAuthRestAPI(GenericRestAPI):

    def __init__(self, username, password):
        super(BasicAuthRestAPI, self).__init__()
        self._session.auth = (username, password)


class TokenBasicAuthRestAPI(BasicAuthRestAPI):

    def __init__(self, token):
        super(TokenBasicAuthRestAPI, self).__init__(username=token, password='')


class BearerTokenRestAPI(GenericRestAPI):

    def __init__(self, token):
        super(BearerTokenRestAPI, self).__init__()
        self._session.headers['Authorization'] = 'Bearer %s' % token


class TokenHeaderRestAPI(GenericRestAPI):

    def __init__(self, token):
        super(TokenHeaderRestAPI, self).__init__()
        self._session.headers['Authorization'] = 'Token %s' % token


class OAuthAPI(BearerTokenRestAPI):

    def __init__(self, access_token):
        super(OAuthAPI, self).__init__(token=access_token)


class RestfulErroringAPI(GenericRestAPI):

    def _request(self, method, url, *args, **kwargs):
        resp = super(RestfulErroringAPI, self)._request(method, url, *args, **kwargs)
        resp.raise_for_status()
        return resp


class JSONAPI(RestfulErroringAPI):

    def _request(self, method, url, *args, **kwargs):
        if not kwargs.pop('raw_json', False):
            kwargs['json'] = kwargs.pop('data', kwargs.get('json', None))
        try:
            kwargs['headers']['Content-Type'] = 'application/json'
        except (KeyError, TypeError):
            kwargs['headers'] = {'Content-Type': 'application/json'}
        with_meta = kwargs.pop('with_meta', False)
        try:
            resp = super(JSONAPI, self)._request(method, url, *args, **kwargs)
            if with_meta:
                return resp
            elif resp.status_code == 204:
                return None
            else:
                return resp.json()
        except HTTPError:
            # In the future we might want to do some custom error handling here
            raise
