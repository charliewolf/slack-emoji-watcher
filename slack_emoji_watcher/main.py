import os.path
import yaml

import telethon.sync  # noqa: F401

from telethon import TelegramClient
from slack_emoji_watcher.api_client import SlackAPI

config_file = os.path.expanduser('~/.slack-emoji-watcher')

with open(config_file, 'r') as fh:
    config = yaml.load(fh)

token = config['slack_token']

reactions_i_care_about = config['reactions']


def main():
    telegram_client = TelegramClient('watcher', config['telegram']['id'], config['telegram']['hash'])
    telegram_client.start(bot_token=config['telegram']['bot_token'])
    api = SlackAPI(token)
    for event_type, event_data in api.rtm_stream(True):
        print(event_data)
        if event_type == 'reaction_added':
            reaction = event_data['reaction']
            print(reaction)
            if reaction in reactions_i_care_about:
                item = event_data['item']
                print(item)
                if item['type'] == 'message':
                    ts = item['ts']
                    channel = item['channel']
                    link = api.get_permalink(channel, ts)
                    message = f"Someone reacted with the the {reaction} emoji at {link}"
                    telegram_client.send_message(config['telegram']['recipient'], message)


if __name__ == '__main__':
    main()
