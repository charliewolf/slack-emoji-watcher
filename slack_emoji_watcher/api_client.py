import datetime
import time
import json
import websocket
import calendar
import enum

from typing import Optional, Iterable, Tuple

from slack_emoji_watcher.util.rest_client.base import GenericRestAPI, HTTPError


class ObjectType(enum.Enum):
    IM = enum.auto()
    Channel = enum.auto()
    User = enum.auto()
    Group = enum.auto()


class SlackAPIException(HTTPError):
    def __init__(self, error, **kwargs):
        self.error = error
        self.args = (error,)
        super(SlackAPIException, self).__init__(**kwargs)

    def __str__(self):
        return self.error


class SlackAPI(GenericRestAPI):
    BASE_URL = "https://api.slack.com/api/"

    def __init__(self, access_token: Optional[str]):
        self.access_token = access_token
        super(SlackAPI, self).__init__()

    @classmethod
    def exchange(cls, client_id: str, client_secret: str, code: str, redirect_uri: Optional[str] = None):
        instance = cls(access_token=None)
        result = instance.post('oauth.access', data={"client_id": client_id, "client_secret": client_secret, "code": code, "redirect_uri": redirect_uri}, no_token=True)
        return result

    def rpc(self, method: str, **data):
        return self.post(method, data=data)

    def _request(self, method: str, url: str, *args, **kwargs):
        data = kwargs.pop('data', dict()).copy()
        data.update(kwargs.pop('params', dict()))
        if not kwargs.pop('no_token', None):
            data['token'] = self.access_token
        with_meta = kwargs.pop('with_meta', False)
        kwargs['data'] = data
        resp = super(SlackAPI, self)._request(method, url, *args, **kwargs)
        if with_meta:
            return resp
        elif resp.status_code == 204:
            return None
        else:
            response_data = resp.json()
            if not response_data.get('ok'):
                raise SlackAPIException(response_data.get('error'), response=resp, request=resp.request)
            del response_data['ok']
            return response_data

    def list_users(self):
        return self.rpc('users.list')['members']

    def rtm_url(self, simple_latest: bool = False, no_unreads: bool = False, mpim_aware: bool = False) -> str:
        return self.rpc('rtm.start')['url']

    def rtm_stream(self, raise_on_error: bool = False):
        url = self.rtm_url()
        while True:
            try:
                ws = websocket.create_connection(url)
                while True:
                    event = json.loads(ws.recv())
                    if event['type'] == 'reconnect_url':
                        url = event['url']
                    yield event['type'], event
            except Exception:
                if raise_on_error:
                    raise

    def list_ims(self) -> Iterable[dict]:
        return self.rpc('im.list')['ims']

    def list_channels(self) -> Iterable[dict]:
        return self.rpc('channels.list')['channels']

    def list_groups(self) -> Iterable[dict]:
        return self.rpc('groups.list')['groups']

    def im_info(self, id) -> Optional[dict]:
        try:
            return [im for im in self.list_ims() if im['id'] == id][0]
        except IndexError:
            return None

    def user_info(self, id) -> dict:
        return self.rpc('users.info', user=id)['user']

    def channel_info(self, id) -> dict:
        return self.rpc('channels.info', channel=id)['channel']

    def group_info(self, id) -> dict:
        return self.rpc('groups.info', group=id)['group']

    def open_im(self, user_id) -> dict:
        return self.rpc('im.open', user=user_id)['channel']['id']

    def post_message(self, channel, text) -> dict:
        return self.rpc('chat.postMessage', channel=channel, text=text, as_user=True)

    def history(self, object_type: ObjectType, id: str, count: int = 100, unreads: bool = False, latest: Optional[float] = None, oldest: float = 0.0) -> Tuple[Iterable[dict], bool]:
        if object_type == ObjectType.Channel:
            rpc_method = 'channels.history'
        elif object_type == ObjectType.Group:
            rpc_method = 'groups.history'
        elif object_type == ObjectType.IM:
            rpc_method = 'im.history'
        elif object_type == ObjectType.User:
            object_type = 'im'
            id = self.open_im(id)
            rpc_method = 'im.history'
        if not latest:
            latest = time.time()
        if isinstance(latest, datetime.datetime):
            latest = calendar.timegm(latest.utctimetuple())
        if isinstance(oldest, datetime.datetime):
            oldest = calendar.timegm(oldest.utctimetuple())
        response = self.rpc(rpc_method, unreads=unreads, count=count, latest=latest, oldest=oldest, channel=id)
        return response['messages'], bool(response.get('has_more'))

    def all_history(self, *args, **kwargs) -> Iterable[dict]:
        has_more = True
        while has_more:
            messages, has_more = self.history(*args, count=1000, **kwargs)
            if len(messages):
                oldest_ts = messages[-1]['ts']
                kwargs['latest'] = float(oldest_ts)
            yield from messages

    def revoke(self) -> bool:
        self.rpc('auth.revoke')
        return True

    def get_permalink(self, channel: str, id: str) -> str:
        return self.rpc('chat.getPermalink', channel=channel, message_ts=id)['permalink']
